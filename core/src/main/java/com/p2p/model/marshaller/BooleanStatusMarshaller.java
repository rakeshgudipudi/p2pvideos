package com.p2p.model.marshaller;

import com.p2p.model.BooleanStatus;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;


/**
 * The Class BooleanStatusMarshaller.
 */
public class BooleanStatusMarshaller implements AttributeConverter<BooleanStatus, String> {

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang.
     * Object)
     */
    @Override
    public String convertToDatabaseColumn(BooleanStatus attribute) {
        if (attribute == null) {
            return BooleanStatus.DISABLED.name();
        }
        return attribute.name();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang.
     * Object)
     */
    @Override
    public BooleanStatus convertToEntityAttribute(String dbData) {
        if (StringUtils.isEmpty(dbData)) {
            return BooleanStatus.DISABLED;
        }
        return BooleanStatus.valueOf(dbData);
    }

}
