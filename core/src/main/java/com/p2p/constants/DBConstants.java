package com.p2p.constants;

/**
 * The Class DBConstants.
 */
public class DBConstants {

    /**
     * The constant COLUMN_CREATED_TIME.
     */
    public static final String COLUMN_CREATED_TIME = "created_time";
    /**
     * The constant COLUMN_LAST_UPDATED_TIME.
     */
    public static final String COLUMN_LAST_UPDATED_TIME = "last_updated";

    /**
     * The constant HIBERNATE_UUID_GENERATOR.
     */
    public static final String HIBERNATE_UUID_GENERATOR = "hibernate-uuid";
    /**
     * The constant HIBERNATE_UUID_GENERATOR_STRATEGY.
     */
    public static final String HIBERNATE_UUID_GENERATOR_STRATEGY = "uuid2";
}
