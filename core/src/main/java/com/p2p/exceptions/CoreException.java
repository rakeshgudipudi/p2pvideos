package com.p2p.exceptions;

/**
 * The type Core exception.
 */
public class CoreException extends RuntimeException {

    private static final long serialVersionUID = -1352741341491250475L;

    /**
     * Instantiates a new Core exception.
     */
    public CoreException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * Instantiates a new Core exception.
     *
     * @param message            the message
     * @param cause              the cause
     * @param enableSuppression  the enable suppression
     * @param writableStackTrace the writable stack trace
     */
    public CoreException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

    /**
     * Instantiates a new Core exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public CoreException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    /**
     * Instantiates a new Core exception.
     *
     * @param message the message
     */
    public CoreException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    /**
     * Instantiates a new Core exception.
     *
     * @param cause the cause
     */
    public CoreException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    /**
     * The type Not valid exception.
     */
    public static class NotValidException extends CoreException {

        /**
         *
         */
        private static final long serialVersionUID = -8891808942835246216L;

        /**
         * Instantiates a new Not valid exception.
         */
        public NotValidException() {
            super();
            // TODO Auto-generated constructor stub
        }

        /**
         * Instantiates a new Not valid exception.
         *
         * @param message            the message
         * @param cause              the cause
         * @param enableSuppression  the enable suppression
         * @param writableStackTrace the writable stack trace
         */
        public NotValidException(String message, Throwable cause, boolean enableSuppression,
                                 boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
            // TODO Auto-generated constructor stub
        }

        /**
         * Instantiates a new Not valid exception.
         *
         * @param message the message
         * @param cause   the cause
         */
        public NotValidException(String message, Throwable cause) {
            super(message, cause);
            // TODO Auto-generated constructor stub
        }

        /**
         * Instantiates a new Not valid exception.
         *
         * @param message the message
         * @param args    the args
         */
        public NotValidException(String message, Object... args) {
            super(String.format(message, args));
        }


        /**
         * Instantiates a new Not valid exception.
         *
         * @param message the message
         */
        public NotValidException(String message) {
            super(message);
            // TODO Auto-generated constructor stub
        }

        /**
         * Instantiates a new Not valid exception.
         *
         * @param cause the cause
         */
        public NotValidException(Throwable cause) {
            super(cause);
            // TODO Auto-generated constructor stub
        }

    }

    /**
     * The type Not found exception.
     */
    public static class NotFoundException extends CoreException {

        /**
         *
         */
        private static final long serialVersionUID = -8891808942835246216L;

        /**
         * Instantiates a new Not found exception.
         */
        public NotFoundException() {
            super();
            // TODO Auto-generated constructor stub
        }

        /**
         * Instantiates a new Not found exception.
         *
         * @param message            the message
         * @param cause              the cause
         * @param enableSuppression  the enable suppression
         * @param writableStackTrace the writable stack trace
         */
        public NotFoundException(String message, Throwable cause, boolean enableSuppression,
                                 boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
            // TODO Auto-generated constructor stub
        }

        /**
         * Instantiates a new Not found exception.
         *
         * @param message the message
         * @param args    the args
         */
        public NotFoundException(String message, Object... args) {
            super(String.format(message, args));
        }

        /**
         * Instantiates a new Not found exception.
         *
         * @param message the message
         * @param cause   the cause
         */
        public NotFoundException(String message, Throwable cause) {
            super(message, cause);
            // TODO Auto-generated constructor stub
        }

        /**
         * Instantiates a new Not found exception.
         *
         * @param message the message
         */
        public NotFoundException(String message) {
            super(message);
            // TODO Auto-generated constructor stub
        }

        /**
         * Instantiates a new Not found exception.
         *
         * @param cause the cause
         */
        public NotFoundException(Throwable cause) {
            super(cause);
            // TODO Auto-generated constructor stub
        }

    }

}
