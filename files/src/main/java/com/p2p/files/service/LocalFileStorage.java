package com.p2p.files.service;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * The Class LocalFileStorage. This class is used to access file system and file handles
 */
@Component
@Lazy
public class LocalFileStorage {

    private static final Logger LOG = Logger.getLogger(LocalFileStorage.class);

    @Value("${com.p2p.files.sharedDirectory}")
    private String sharedDirectory;
    @Value("${com.p2p.files.downloadDirectory}")
    private String downloadDirectory;

    /**
     * The Required extensions.
     */
    @Value("#{'${com.p2p.files.extensions}'.split(',')}")
    List<String> requiredExtensions;


    private File createFile(String fileName) {
        if (StringUtils.isEmpty(fileName)) {
            throw new IllegalArgumentException("File name cannot be empty");
        }
        return new File(fileName);
    }

    /**
     * List files in the given directory. It will filter out the files given by fileFilter and directoryFileter
     *
     * @param directory      the directory
     * @param fileFilter     the file filter
     * @param directorFilter the director filter
     * @return the list
     */
    public List<File> listFiles(File directory, IOFileFilter fileFilter, IOFileFilter directorFilter) {
        return new ArrayList<>(FileUtils.listFiles(directory, fileFilter, directorFilter));
    }

    /**
     * Gets shared directory.
     *
     * @return the shared directory
     */
    public File getSharedDirectory() {
        return FileUtils.getFile(sharedDirectory);
    }

    /**
     * Gets download directory.
     *
     * @return the download directory
     */
    public File getDownloadDirectory() {
        return FileUtils.getFile(downloadDirectory);
    }

    /**
     * Gets required extensions.
     *
     * @return the required extensions
     */
    public List<String> getRequiredExtensions() {
        LOG.error(requiredExtensions);
        return requiredExtensions;
    }
}
