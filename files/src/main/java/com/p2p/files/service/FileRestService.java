package com.p2p.files.service;

import com.p2p.files.models.UploadedFile;
import com.p2p.files.models.FileChunk;
import com.p2p.model.BooleanStatus;
import com.p2p.peers.model.Peer;
import com.p2p.peers.service.PeerRestService;
import okhttp3.ResponseBody;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The type Peer rest service. This class is used to contact other peers and retrieve appropriate information
 */
@Component
public class FileRestService extends PeerRestService {

    private static final Logger LOG = Logger.getLogger(FileRestService.class);

    private FileApi peerApi;

    @Override
    protected void initializeService() {
        this.peerApi = getRestClient().create(FileApi.class);
    }

    /**
     * Gets files by hash from network.
     *
     * @param filesHashes     the files hashes
     * @param peerStatus      the peer status
     * @param streamingStatus the streaming status
     * @return the files by hash from network
     */
    public Map<Peer, List<UploadedFile>> getFilesByHashFromNetwork(List<String> filesHashes, BooleanStatus peerStatus,
                                                                   BooleanStatus streamingStatus) {
        return makeRequestToAllPeers(peerApi.getFilesByHashFromNetwork(filesHashes), peerStatus, streamingStatus);
    }

    /**
     * Gets uploaded file chunks by file hash.
     *
     * @param fileHash the file hash
     * @param peer     the peer
     * @return the uploaded file chunks by file hash
     */
    public List<FileChunk> getUploadedFileChunksByFileHash(String fileHash, Peer peer) {
        return makeRequestToPeer(peer, peerApi.getUploadedFileChunksByFileHash(fileHash));
    }

    /**
     * Gets uploaded file chunk by chunk hash.
     *
     * @param chunkHash the chunk hash
     * @param peer      the peer
     * @return the uploaded file chunk by chunk hash
     */
    public FileChunk getUploadedFileChunkByChunkHash(String chunkHash, Peer peer) {
        return makeRequestToPeer(peer, peerApi.getUploadedFileChunkByChunkHash(chunkHash));
    }

    /**
     * Gets uploaded file chunk from network.
     *
     * @param chunkHash       the chunk hash
     * @param peerStatus      the peer status
     * @param streamingStatus the streaming status
     * @return the uploaded file chunk from network
     */
    public Map<Peer, FileChunk> getUploadedFileChunkFromNetwork(String chunkHash, BooleanStatus peerStatus,
                                                                BooleanStatus streamingStatus) {
        return makeRequestToAllPeers(peerApi.getUploadedFileChunkByChunkHash(chunkHash), peerStatus, streamingStatus);
    }

    /**
     * Download file from peer input stream.
     *
     * @param chunkHash the chunk hash
     * @param peer      the peer
     * @return the input stream
     */
    public InputStream downloadFileFromPeer(String chunkHash, Peer peer) {
        Map<String, Object> requestParamaters = new HashMap<>();
        requestParamaters.put("chunk_hash", chunkHash);
        return makeRequestToPeer(peer, peerApi.downloadFileChunk(requestParamaters)).byteStream();
    }

    /**
     * The interface Peer api.Internal implementaion to access other peers using HTTP
     */
    public interface FileApi {

        /**
         * Gets files by hash from network.
         *
         * @param fileHashes the file hashes
         * @return the files by hash from network
         */
        @GET("files/hashes")
        Call<List<UploadedFile>> getFilesByHashFromNetwork(@Query("hash") List<String> fileHashes);

        /**
         * Gets all files.
         *
         * @param status the status
         * @return the all files
         */
        @GET("files")
        Call<List<UploadedFile>> getAllFiles(@Query("status") String status);

        /**
         * Gets uploaded file chunks by file hash.
         *
         * @param fileHash the file hash
         * @return the uploaded file chunks by file hash
         */
        @GET("files/chunks")
        Call<List<FileChunk>> getUploadedFileChunksByFileHash(@Query("hash") String fileHash);

        /**
         * Gets uploaded file chunk by chunk hash.
         *
         * @param chunkHash the chunk hash
         * @return the uploaded file chunk by chunk hash
         */
        @GET("files/chunks/{chunkHash}")
        Call<FileChunk> getUploadedFileChunkByChunkHash(@Path("chunkHash") String chunkHash);

        /**
         * Download file chunk call.
         *
         * @param requestParameters the request parameters
         * @return the call
         */
        @POST("files/chunks/download")
        @Streaming
        Call<ResponseBody> downloadFileChunk(@Body Map<String, Object> requestParameters);
    }
}
