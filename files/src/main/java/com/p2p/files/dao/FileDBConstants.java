package com.p2p.files.dao;

/**
 * The type File db constants.
 */
public class FileDBConstants {

    /**
     * The constant TABLE_FILES.
     */
    public static final String TABLE_FILES = "files";
    /**
     * The constant TABLE_FILE_CHUNKS.
     */
    public static final String TABLE_FILE_CHUNKS = "file_chunks";

    /**
     * The constant COLUMN_FILES_ID.
     */
    public static final String COLUMN_FILES_ID = "file_id";
    /**
     * The constant COLUMN_FILES_NAME.
     */
    public static final String COLUMN_FILES_NAME = "file_name";
    public static final String COLUMN_FILES_PATH = "file_path";
    /**
     * The constant COLUMN_FILES_SIZE.
     */
    public static final String COLUMN_FILES_SIZE = "file_size";
    /**
     * The constant COLUMN_FILES_MD5_HASH.
     */
    public static final String COLUMN_FILES_MD5_HASH = "file_hash";
    /**
     * The constant COLUMN_FILES_STATUS.
     */
    public static final String COLUMN_FILES_STATUS = "status";

    /**
     * The constant COLUMN_FILE_CHUNKS_ID.
     */
    public static final String COLUMN_FILE_CHUNKS_ID = "file_chunk_id";
    /**
     * The constant COLUMN_FILE_CHUNKS_FILE_ID.
     */
    public static final String COLUMN_FILE_CHUNKS_FILE_ID = "file_id";
    public static final String COLUMN_FILE_CHUNKS_OFFSET = "file_offset";
    public static final String COLUMN_FILE_CHUNKS_LENGTH = "length";
    public static final String COLUMN_FILE_CHUNKS_TYPE = "chunk_type";
    public static final String COLUMN_FILE_CHUNKS_PATH = "chunk_path";
    /**
     * The constant COLUMN_FILE_CHUNKS_STATUS.
     */
    public static final String COLUMN_FILE_CHUNKS_STATUS = "status";
    /**
     * The constant COLUMN_FILE_CHUNKS_MD5_HASH.
     */
    public static final String COLUMN_FILE_CHUNKS_MD5_HASH = "file_chunk_hash";
}
