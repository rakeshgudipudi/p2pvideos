package com.p2p.files.dao;

import com.p2p.dao.AbstractDao;
import com.p2p.files.models.UploadedFile;
import com.p2p.model.BooleanStatus;
import com.p2p.utils.DateTimeUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * The type Uploaded file dao. This class is used to access database table @{@link FileDBConstants#TABLE_FILES}
 */
@Repository("uploadedFileDao")
public class UploadedFileDao extends AbstractDao {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private DateTimeUtils dateTimeUtils;

    @PostConstruct
    private void initializeDao() {
        setSessionFactory(sessionFactory);
        setDateTimeUtils(dateTimeUtils);
    }

    /**
     * Gets file by hash.
     *
     * @param hash the hash
     * @return the file by hash
     */
    public UploadedFile getFileByHash(String hash) {
        Session session = getCurrentSession();
        Criteria completeCriteria = session.createCriteria(UploadedFile.class);

        completeCriteria.add(Restrictions.eq("fileHash", hash));

        return (UploadedFile) completeCriteria.uniqueResult();
    }

    /**
     * Gets files by hash.
     *
     * @param fileHashes the file hashes
     * @return the files by hash
     */
    @SuppressWarnings("unchecked")
    public List<UploadedFile> getFilesByHash(List<String> fileHashes) {
        Session session = getCurrentSession();
        Criteria completeCriteria = session.createCriteria(UploadedFile.class);

        completeCriteria.add(Restrictions.in("fileHash", fileHashes));

        return completeCriteria.list();
    }

    /**
     * Gets all files.
     *
     * @param status the status
     * @return the all files
     */
    @SuppressWarnings("unchecked")
    public List<UploadedFile> getAllFiles(BooleanStatus status) {
        Session session = getCurrentSession();
        Criteria completeCriteria = session.createCriteria(UploadedFile.class);

        if (status != null) {
            completeCriteria.add(Restrictions.eq("status", status));
        }

        return completeCriteria.list();
    }
}
