package com.p2p.files.utils;

import com.p2p.exceptions.CoreException;
import com.p2p.files.models.FileRange;
import com.p2p.files.service.config.FilesConfiguration;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * The type File io utils. Utility class to create File handles and other functions
 */
@Component("fileIOUtils")
public class FileIOUtils {

    @Autowired
    private FilesConfiguration filesConfiguration;

    private static final Logger LOG = Logger.getLogger(FileIOUtils.class);


    /**
     * Gets file.
     *
     * @param fileName     the file name
     * @param shouldCreate the should create
     * @return the file
     * @throws IOException the io exception
     */
    public File getFile(String fileName, boolean shouldCreate) throws IOException {
        File file = FileUtils.getFile(fileName);
        if (!file.exists() && shouldCreate) {
            file.createNewFile();
        }
        return file;
    }

    /**
     * Gets directory.
     *
     * @param directory the directory
     * @return the directory
     * @throws IOException the io exception
     */
    public File getDirectory(String directory) throws IOException {
        File file = getFile(directory, true);
        if (!file.isDirectory()) {
            throw new CoreException.NotValidException("directory name expected. But file name given " + directory);
        }
        return file;
    }

    /**
     * Gets file size.
     *
     * @param file the file
     * @return the file size
     */
    public long getFileSize(File file) {
        return FileUtils.sizeOf(file);
    }

    /**
     * Decide chunk size long.
     *
     * @param fileSize the file size
     * @param chunks   the chunks
     * @return the long
     */
    public long decideChunkSize(long fileSize, int chunks) {
        return fileSize / chunks;
    }

    /**
     * Calculate file ranges list.
     *
     * @param fileSize  the file size
     * @param chunkSize the chunk size
     * @return the list
     */
    public List<FileRange> calculateFileRanges(long fileSize, long chunkSize) {
        List<FileRange> fileRanges = new ArrayList<>();
        fileRanges.add(new FileRange(0, filesConfiguration.firstChunkSize));
        for (long currentOffset = filesConfiguration.firstChunkSize; currentOffset < fileSize; ) {
            long currentChunkSize = currentOffset + chunkSize > fileSize ? fileSize - currentOffset : chunkSize;
            fileRanges.add(new FileRange(currentOffset, currentChunkSize));
            currentOffset = currentOffset + currentChunkSize;
        }
        return fileRanges;
    }

    /**
     * Gets file ranges.
     *
     * @param file the file
     * @return the file ranges
     */
    public List<FileRange> getFileRanges(File file) {
        long fileSize = getFileSize(file);
        long chunkSize = decideChunkSize(fileSize, filesConfiguration.minChunks);
        return calculateFileRanges(fileSize, chunkSize);
    }

    /**
     * Gets file.
     *
     * @param fileName the file name
     * @return the file
     */
    public File getFile(String fileName) {
        return FileUtils.getFile(fileName);
    }

    /**
     * Gets file md 5.
     *
     * @param file the file
     * @return the file md 5
     * @throws IOException the io exception
     */
    public String getFileMd5(File file) throws IOException {
        InputStream stream = FileUtils.openInputStream(file);
        String md5 = getFileMd5(stream);
        closeSilently(stream);
        return md5;
    }

    /**
     * Gets file md 5.
     *
     * @param inputStream the input stream
     * @return the file md 5
     * @throws IOException the io exception
     */
    public String getFileMd5(InputStream inputStream) throws IOException {
        return DigestUtils.md5Hex(inputStream);
    }

    /**
     * Close silently.
     *
     * @param closeable the closeable
     */
    public void closeSilently(Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        } catch (Exception e) {

        }
    }

    /**
     * Gets md 5 hash for range.
     *
     * @param file      the file
     * @param fileRange the file range
     * @return the md 5 hash for range
     * @throws IOException the io exception
     */
    public String getMD5HashForRange(File file, FileRange fileRange) throws IOException {
        File temporaryFile = getTemporaryFile(file, fileRange);
        String fileHash = getFileMd5(temporaryFile);
        FileUtils.forceDelete(temporaryFile);
        return fileHash;
    }

    /**
     * Gets temporary file.
     *
     * @param file      the file
     * @param fileRange the file range
     * @return the temporary file
     * @throws IOException the io exception
     */
    public File getTemporaryFile(File file, FileRange fileRange) throws IOException {
        File temporaryFile = File.createTempFile(file.getName(), RandomStringUtils.randomAlphanumeric(5));
        OutputStream temporaryStream = FileUtils.openOutputStream(temporaryFile);
        InputStream inputStream = FileUtils.openInputStream(file);
        long copiedBytes = copyLarge(inputStream, temporaryStream, fileRange);
        closeSilently(inputStream);
        closeSilently(temporaryStream);
        return temporaryFile;
    }

    /**
     * Copy large long.
     *
     * @param inputStream  the input stream
     * @param outputStream the output stream
     * @param fileRange    the file range
     * @return the long
     * @throws IOException the io exception
     */
    public long copyLarge(InputStream inputStream, OutputStream outputStream, FileRange fileRange)
            throws IOException {
        return IOUtils
                .copyLarge(inputStream, outputStream, fileRange.getOffset(), fileRange.getSize());
    }

    /**
     * Split file list.
     *
     * @param file            the file
     * @param outputDirectory the output directory
     * @param fileRanges      the file ranges
     * @return the list
     * @throws IOException the io exception
     */
    public List<File> splitFile(File file, String outputDirectory, List<FileRange> fileRanges) throws IOException {
        String fileName = FilenameUtils.getName(file.getAbsolutePath());
        List<File> outputFiles = new ArrayList<>();
        for (int i = 0; i < fileRanges.size(); i++) {

            InputStream fileInputStream = FileUtils.openInputStream(file);
            String chunkedFileName = getChunkedFileName(fileName, i + 1);
            File outputChunk = FileUtils.getFile(FilenameUtils.concat(outputDirectory, chunkedFileName));
            OutputStream chunkOutputStream = FileUtils.openOutputStream(outputChunk);
            copyLarge(fileInputStream, chunkOutputStream, fileRanges.get(i));
            closeSilently(chunkOutputStream);
            closeSilently(fileInputStream);
            outputFiles.add(outputChunk);
        }
        return outputFiles;
    }

    /**
     * Join files.
     *
     * @param files      the files
     * @param outputFile the output file
     * @throws IOException the io exception
     */
    public void joinFiles(List<File> files, File outputFile) throws IOException {
        FileOutputStream outputStream = FileUtils.openOutputStream(outputFile, true);
        files.forEach(file -> {
            try {
                FileUtils.writeByteArrayToFile(outputFile, FileUtils.readFileToByteArray(file), true);
            } catch (IOException e) {
                throw new CoreException.NotValidException(e.getMessage(), e);
            }
        });
        closeSilently(outputStream);
    }


    private String getChunkedFileName(String fileName, int chunkNumber) {
        return fileName + "." + chunkNumber;
    }
}
