package com.p2p.peers.dao;

/**
 * The type Peer db constants.
 */
public class PeerDBConstants {

    /**
     * The constant TABLE_PEERS.
     */
    public static final String TABLE_PEERS = "peers";

    /**
     * The constant COLUMN_PEER_ID.
     */
    public static final String COLUMN_PEER_ID = "peer_id";
    /**
     * The constant COLUMN_PEER_NAME.
     */
    public static final String COLUMN_PEER_NAME = "name";
    /**
     * The constant COLUMN_PEER_IP.
     */
    public static final String COLUMN_PEER_IP = "ip";
    /**
     * The constant COLUMN_PEER_PROTOCOL.
     */
    public static final String COLUMN_PEER_PROTOCOL = "protocol";
    /**
     * The constant COLUMN_PEER_PORT.
     */
    public static final String COLUMN_PEER_PORT = "port";
    /**
     * The constant COLUMN_PEER_ONLINE.
     */
    public static final String COLUMN_PEER_ONLINE = "online";
    /**
     * The constant COLUMN_PEER_STREAMING.
     */
    public static final String COLUMN_PEER_STREAMING = "streaming";

}
