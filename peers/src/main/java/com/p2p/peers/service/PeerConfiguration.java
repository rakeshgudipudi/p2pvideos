package com.p2p.peers.service;

import java.util.List;

import com.p2p.model.BooleanStatus;
import com.p2p.peers.model.Peer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * The type Peer configuration.
 */
@Configuration
@PropertySource("classpath:peers.properties")
public class PeerConfiguration {

    /**
     * The Known peers.
     */
    @Value("#{'${com.peer.knownpeers}'.split(',')}")
    List<String> knownPeers;

    /**
     * The Current peer ip.
     */
    @Value("#{'${com.peer.ip}'}")
    String currentPeerIp;
    /**
     * The Current peer port.
     */
    @Value("#{'${com.peer.port}'}")
    String currentPeerPort;
    /**
     * The Current peer name.
     */
    @Value("#{'${com.peer.name}'}")
    String currentPeerName;

    /**
     * Gets own peer.
     *
     * @return the own peer
     */
    public Peer getOwnPeer() {
        Peer peer = new Peer();
        peer.setName(currentPeerName);
        peer.setIp(currentPeerIp);
        peer.setPort(currentPeerPort);
        peer.setOnline(BooleanStatus.ACTIVE);
        peer.setStreaming(BooleanStatus.DISABLED);

        return peer;
    }

}
