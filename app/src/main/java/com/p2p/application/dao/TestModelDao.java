package com.p2p.application.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.p2p.application.model.TestModel;

/**
 * The type Test model dao.
 */
@Repository("testModelDao")
public class TestModelDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Create test model string.
     *
     * @param testModel the test model
     * @return the string
     */
    public String createTestModel(TestModel testModel) {
        Session session = sessionFactory.getCurrentSession();
        return (String) session.save(testModel);
    }

    /**
     * Get test model.
     *
     * @param id the id
     * @return the test model
     */
    public TestModel get(String id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(TestModel.class, id);
    }
}
