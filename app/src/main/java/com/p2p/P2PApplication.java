package com.p2p;

import com.p2p.guifx.SceneController;
import com.p2p.guifx.loader.FXMLLoaderService;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.h2.server.web.WebServlet;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * The Class Application.
 */
@Configuration
@SpringBootApplication(scanBasePackages = "com.p2p")
@ImportResource("classpath:spring/application-context.xml")
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class P2PApplication extends Application {

    private static String[] args;
    private static ConfigurableApplicationContext context;

    /**
     * Tomcat embedded servlet container factory tomcat embedded servlet container factory.
     *
     * @return the tomcat embedded servlet container factory
     */
    @Bean
    public TomcatEmbeddedServletContainerFactory tomcatEmbeddedServletContainerFactory() {
        return new TomcatEmbeddedServletContainerFactory();
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {
        P2PApplication.args = args;
        context = new SpringApplicationBuilder(P2PApplication.class)
                .headless(false).run(args);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    Thread.sleep(200);
                    context.close();
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        launch(args);

    }

    @Override
    public void start(final Stage stage) throws Exception {
        //Parent root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        // Bootstrap Spring context here.


        FXMLLoaderService loaderService = context.getBean(FXMLLoaderService.class);
        SceneController sceneController = context.getBean(SceneController.class);
        FXMLLoader loader = loaderService.load("mainpage.fxml");
        Parent root = loader.load();
        Scene scene = sceneController.createScene(root);
        stage.setTitle("JavaFX and Maven");
        stage.setScene(scene);
        stage.show();
    }


    /**
     * H 2 servlet registration servlet registration bean.
     *
     * @return the servlet registration bean
     */
    @Bean
    public ServletRegistrationBean h2servletRegistration() {
        ServletRegistrationBean registration = new ServletRegistrationBean(new WebServlet());
        registration.addUrlMappings("/console/*");
        return registration;
    }

}
